<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::get();
        return view('contacts.index', compact('contacts'));
    }

    public function store()
    {
        Contact::create([
            'name' => 'Mawuko'
        ]);

        return back();
    }
}
